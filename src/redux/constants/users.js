export const RECEIVE_USERS = "RECEIVE_USERS";
export const SET_IS_FETCHING = "SET_IS_FETCHING";
export const DELETE_USER = "DELETE_USER";
export const ADD_USER = "ADD_USER";
export const EDIT_USER = "EDIT_USER";
