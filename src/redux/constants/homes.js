export const RECEIVE_HOMES = "RECEIVE_HOMES";
export const SET_IS_FETCHING = "SET_IS_FETCHING";
export const DELETE_HOME = "DELETE_HOME";
export const ADD_HOME = "ADD_HOME";
export const EDIT_HOME = "EDIT_HOME";
