export const receiveUsers = () => {
  return fetch("http://localhost:3004/users")
}

export const deleteUser = (id) => {
  return fetch(`http://localhost:3004/users/${id}`, {method: "DELETE" })
}

export const addUser = (data) => {
  return fetch("http://localhost:3004/users", {
      method: "POST",
      headers: {
      'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
  })
}

export const editUser = (id, data) => {
  return fetch(`http://localhost:3004/users/${id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
}