export const receiveHomes = () => {
  return fetch("http://localhost:3004/homes")
}

export const deleteHome = (id) => {
  return fetch(`http://localhost:3004/homes/${id}`, {method: "DELETE" })
}

export const addHome = (data) => {
  return fetch("http://localhost:3004/homes", {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
}

export const editHome = (id, data) => {
  return fetch(`http://localhost:3004/homes/${id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
}