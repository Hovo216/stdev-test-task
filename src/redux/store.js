import { createStore, applyMiddleware  } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import reducers from "./reducers/root";

export default (preloadedState) =>
  createStore(
    reducers,
    preloadedState,
    composeWithDevTools(applyMiddleware(thunk))
  );