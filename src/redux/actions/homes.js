import { RECEIVE_HOMES, SET_IS_FETCHING, DELETE_HOME, ADD_HOME, EDIT_HOME} from "../constants/homes";

import * as API from "../API/homes_api"

export const receiveHomes = (homes) => ({
  type: RECEIVE_HOMES,
  homes
})

export const setIsFetching = (bool) => ({
  type: SET_IS_FETCHING,
  bool
})

export const deleteHome = (id) => ({
  type: DELETE_HOME,
  id
})

export const addHome = (data) => ({
  type: ADD_HOME,
  data
})

export const editHome = (id, data) => ({
  type: EDIT_HOME,
  id,
  data,
})

export const receive_homes = () => async dispatch => {
  try {
    dispatch(setIsFetching(true))
    const response = await API.receiveHomes();
    const data = await response.json();

    dispatch(receiveHomes(data))
    dispatch(setIsFetching(false))
    
  } catch (err) {
    alert("Can't receive homes. Pleace reload page.");
    console.log(err)
  }
}

export const delete_home = (id) => async dispatch => {
  try {
    await API.deleteHome(id);
    dispatch(deleteHome(id))
  } catch (err) {
    alert("Can't delete home. Pleace try again.");
    console.log(err)
  }
}

export const add_home = (data) => async dispatch => {
  try {
    await API.addHome(data)
    dispatch(addHome(data))
  } catch (err) {
    alert("Can't add home. Pleace try again.");
    console.log(err)
  }
}

export const edit_home = (id, data) => async dispatch => {
  try {
    await API.editHome(id, data)
    dispatch(editHome(data))
  } catch (err) {
    alert("Can't edit home. Pleace try again.");
    console.log(err)
  }
}