import {
  RECEIVE_USERS,
  SET_IS_FETCHING,
  DELETE_USER,
  ADD_USER,
  EDIT_USER,
} from "../constants/users";

import * as API from "../API/users_api";

export const receiveUsers = (users) => ({
  type: RECEIVE_USERS,
  users,
});

export const setIsFetching = (bool) => ({
  type: SET_IS_FETCHING,
  bool,
});

export const deleteUser = (id) => ({
  type: DELETE_USER,
  id,
});

export const addUser = (data) => ({
  type: ADD_USER,
  data,
});

export const editUser = (id, data) => ({
  type: EDIT_USER,
  id,
  data,
});

export const receive_users = () => async (dispatch) => {
  try {
    dispatch(setIsFetching(true));
    const response = await API.receiveUsers();
    const data = await response.json();

    dispatch(receiveUsers(data));
    dispatch(setIsFetching(false));
  } catch (err) {
    alert("Can't receive users. Pleace reload page.");
    console.log(err);
  }
};

export const delete_user = (id) => async (dispatch) => {
  try {
    await API.deleteUser(id);
    dispatch(deleteUser(id));
  } catch (err) {
    alert("Can't delete user. Pleace try again.");
    console.log(err);
  }
};

export const add_user = (data) => async (dispatch) => {
  try {
    const response = await API.addUser(data);
    dispatch(addUser(data));
  } catch (err) {
    alert("Can't add user. Pleace try again.");
    console.log(err);
  }
};

export const edit_user = (id, data) => async (dispatch) => {
  try {
    const response = await API.editUser(id, data);
    if (response.status === 200) {
      dispatch(editUser(id, data));
    }
  } catch (err) {
    alert("Can't edit user. Pleace try again.");
    console.log(err);
  }
};
