import {combineReducers} from "redux";

import users from "./users";
import homes from "./homes";

export default combineReducers({
  users,
  homes
});
