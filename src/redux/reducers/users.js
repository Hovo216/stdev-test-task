import {
  RECEIVE_USERS,
  SET_IS_FETCHING,
  DELETE_USER,
  ADD_USER,
  EDIT_USER,
} from "../constants/users.js";

const initState = {
  users: [],
  is_fetching: false,
};

export default (state = initState, action) => {
  console.log(action);
  switch (action.type) {
    case RECEIVE_USERS: {
      return {
        ...state,
        users: action.users,
      };
    }
    case SET_IS_FETCHING: {
      return {
        ...state,
        is_fetching: action.bool,
      };
    }
    case DELETE_USER: {
      return {
        ...state,
        users: state.users.filter((user) => user.id !== action.id),
      };
    }
    case ADD_USER: {
      return {
        ...state,
        users: [...state.users, action.data],
      };
    }
    case EDIT_USER: {
      return {
        ...state,
        users: state.users.map((x) => {
          if (action.id === x.id) {
            return {
              ...action.data,
            };
          }
          return x;
        }),
      };
    }
    default:
      return state;
  }
};
