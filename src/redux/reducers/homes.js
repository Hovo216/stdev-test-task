import {
  RECEIVE_HOMES,
  SET_IS_FETCHING,
  DELETE_HOME,
  ADD_HOME,
  EDIT_HOME,
} from "../constants/homes";

const initState = {
  homes: [],
  is_fetching: false,
};

export default (state = initState, action) => {
  switch (action.type) {
    case RECEIVE_HOMES: {
      return {
        ...state,
        homes: action.homes,
      };
    }
    case SET_IS_FETCHING: {
      return {
        ...state,
        is_fetching: action.bool,
      };
    }
    case DELETE_HOME: {
      return {
        ...state,
        homes: state.homes.filter((item) => item.id !== action.id),
      };
    }
    case ADD_HOME: {
      return {
        ...state,
        homes: [...state.homes, action.data],
      };
    }
    case EDIT_HOME: {
      return {
        ...state,
        homes: state.homes.map((x) => {
          if (action.id === x.id) {
            return {
              ...action.data,
            };
          }
          return x;
        }),
      };
    }
    default:
      return state;
  }
};
