import React from "react";

import { useEffect } from "react";
import { connect } from "react-redux";

import {receive_users, delete_user} from "../../redux/actions/users"

import Users from "./view"

const UsersContainer = (props) => {
  useEffect(() => {
    props.receive_users()
  }, [])

  const deleteUser = (id) => {
    props.delete_user(id)
  }

  return (
    <Users users={props.users} is_fetching={props.is_fetching} deleteUser={deleteUser}/>
  )
}

const mapStateToProps = (state) => {
  return {
    users: state.users.users,
    is_fetching: state.users.is_fetching
  }
}
export default connect(mapStateToProps, {receive_users, delete_user})(UsersContainer)
