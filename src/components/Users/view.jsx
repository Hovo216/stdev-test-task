import React from "react";
import { NavLink } from "react-router-dom";

import preloader from "../../static/preloader.svg";
import Styles from "./styles.module.css";

import deleteIcon from "../../static/delete.png";

const Users = (props) => {
  const tableItem = props.users.map((item) => {
    return (
      <tr key={`${item.id} + ${item.firstName}`}>
        <td>{item.firstName}</td>
        <td>{item.lastName}</td>
        <td>{item.email}</td>
        <td>
          <div className="d-flex justify-content-around">
            <NavLink to={`/edit?user?${item.id}`}>Edit</NavLink>
            <button
              onClick={() => props.deleteUser(item.id)}
              className={`${Styles.delete} btn btn-danger`}
            ></button>
          </div>
        </td>
      </tr>
    );
  });
  return (
    <div>
      {props.is_fetching ? (
        <div className={`d-flex align-items-center justify-content-center`}>
          <img src={preloader} alt="preloader" />{" "}
        </div>
      ) : (
        <table className="table">
          <thead>
            <tr>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email</th>
              <th scope="col">Actions</th>
              <th scope="col">
                <NavLink to={"/add?user"} className={Styles.add}>
                  Add User
                </NavLink>
              </th>
            </tr>
          </thead>
          <tbody>{tableItem}</tbody>
        </table>
      )}
    </div>
  );
};

export default Users;
