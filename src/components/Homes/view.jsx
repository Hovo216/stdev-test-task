import React from "react";
import { NavLink } from "react-router-dom";

import preloader from "../../static/preloader.svg";

import Styles from "./styles.module.css";

const Homes = (props) => {
  const tableItem = props.homes.map((item) => {
    return (
      <tr key={`${item.id} + ${item.title}`}>
        <th scope="row">1</th>
        <td>{item.title}</td>
        <td>{item.location}</td>
        <td>{item.landSqm}</td>
        <td>{item.placeSqm}</td>
        <td>{item.bedrooms}</td>
        <td>
          <div className="d-flex justify-content-around">
            <NavLink to={`/edit?home?${item.id}`}>Edit</NavLink>
            <button
              onClick={() => props.deleteHome(item.id)}
              className={`${Styles.delete} btn btn-danger`}
            ></button>
          </div>
        </td>
      </tr>
    );
  });
  return (
    <div>
      {props.is_fetching ? (
        <div className={`d-flex align-items-center justify-content-center`}>
          <img src={preloader} alt="preloader" />{" "}
        </div>
      ) : (
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Title</th>
              <th scope="col">Location</th>
              <th scope="col">Land sqm.</th>
              <th scope="col">Place sqm.</th>
              <th scope="col">Badrooms</th>
              <th scope="col">Actions</th>
              <th scope="col">
                <NavLink to={"/add?home"}>Add Home</NavLink>
              </th>
            </tr>
          </thead>
          <tbody>{tableItem}</tbody>
        </table>
      )}
    </div>
  );
};

export default Homes;
