import React from "react";
import { useEffect } from "react";
import { connect } from "react-redux";

import Homes from "./view"

import {receive_homes, delete_home} from "../../redux/actions/homes"

const HomesContainer = (props) => {
  useEffect(() => {
    props.receive_homes()
  }, [])

  const deleteHome = (id) => {
    props.delete_home(id)
  }

  return (
    <Homes homes={props.homes} is_fetching={props.is_fetching} deleteHome={deleteHome}/>
  )
}

const mapStateToProps = (state) => {
  return {
    homes: state.homes.homes,
    is_fetching: state.homes.is_fetching,
    need_update: state.homes.need_update
  }
}
export default connect(mapStateToProps, {receive_homes, delete_home})(HomesContainer)