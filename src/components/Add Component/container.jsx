import React from "react";
import { Redirect, useHistory } from "react-router-dom";
import { connect } from "react-redux";

import AddComponent from "./view";

import { add_home } from "../../redux/actions/homes";
import { add_user } from "../../redux/actions/users";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { useEffect } from "react";

import { receive_users } from "../../redux/actions/users";

const AddComponentContainer = (props) => {
  useEffect(() => {
    props.receive_users();
  }, []);

  const history = useHistory();

  const addHome = (data) => {
    props.add_home(data);
    history.push("/homes");
  };

  const addUser = (data) => {
    props.add_user(data);
    history.push("/users");
  };

  const came_from = props.location.search;

  return (
    <AddComponent
      users={props.users}
      addHome={addHome}
      addUser={addUser}
      came_from={came_from}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    users: state.users.users,
  };
};

export default compose(
  connect(mapStateToProps, { add_home, add_user, receive_users }),
  withRouter
)(AddComponentContainer);
