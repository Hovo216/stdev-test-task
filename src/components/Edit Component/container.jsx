import React from "react";
import { Redirect, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { useEffect } from "react";

import { edit_home } from "../../redux/actions/homes";
import { edit_user } from "../../redux/actions/users";

import EditComponent from "./view";

import { receive_users } from "../../redux/actions/users";

const EditComponentContainer = (props) => {
  useEffect(() => {
    props.receive_users();
  }, []);

  const history = useHistory();

  const editHome = (id, data) => {
    props.edit_home(id, data);
    history.push("/homes");
  };

  const editUser = (id, data) => {
    props.edit_user(id, data);
    history.push("/users");
  };

  const came_from = props.location.search.split("?")[1];
  const userId = parseInt(props.location.search.split("?")[2]);

  return (
    <EditComponent
      users={props.users}
      editHome={editHome}
      editUser={editUser}
      came_from={came_from}
      userId={userId}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    users: state.users.users,
  };
};

export default compose(
  connect(mapStateToProps, {
    edit_home,
    edit_user,
    receive_users,
  }),
  withRouter
)(EditComponentContainer);
