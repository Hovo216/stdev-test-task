import React from "react";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";

import Styles from "./styles.module.css";

const EditComponent = (props) => {
  const users = props.users.map((item, index) => (
    <option key={item.id} value={`${item.firstName} ${item.lastName}`}>
      {`${item.firstName} ${item.lastName}`}
    </option>
  ));

  const AddHomeSchema = Yup.object().shape({
    userId: Yup.string().required("Required"),
    title: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
    location: Yup.string()
      .min(18, "Too Short!")
      .max(20, "Too Long!")
      .required("Required"),
    land: Yup.number()
      .min(1, "Land square can't be smaller than 1")
      .required("Required"),
    place: Yup.number()
      .min(1, "Place square can't be smaller than 1")
      .required("Required"),
    bedrooms: Yup.number()
      .min(1, "Count of bedrooms can't be smaller than 1")
      .required("Required"),
  });

  const AddUserSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
    lastName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
    age: Yup.number()
      .min(18, "You must be at least 18 years")
      .max(60, "You must be at most 60 years")
      .required("Please supply your age"),
    email: Yup.string().email("*Invalid email").required("*Email is required"),
    password: Yup.string()
      .min(6, "Too Short!")
      .max(12, "Too Long!")
      .required("*Password is required"),
    confirmPass: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });

  return (
    <div style={{ marginTop: "20px" }}>
      {props.came_from === "user" ? (
        <Formik
          initialValues={{
            firstName: "",
            lastName: "",
            age: "",
            email: "",
            password: "",
            confirmPass: "",
          }}
          enableReinitialize
          onSubmit={(values) => {
            props.editUser(props.userId, values);
          }}
          validationSchema={AddUserSchema}
        >
          {({ errors, touched }) => (
            <Form className={Styles.form}>
              {errors.firstName && touched.firstName ? (
                <div className={Styles.error_msg}>{errors.firstName}</div>
              ) : null}
              <Field
                id="firstName"
                name="firstName"
                placeholder="*First Name"
                type="text"
                className={Styles.field}
              />
              {errors.lastName && touched.lastName ? (
                <div className={Styles.error_msg}>{errors.lastName}</div>
              ) : null}
              <Field
                id="lastName"
                name="lastName"
                placeholder="*Last Name"
                type="text"
                className={Styles.field}
              />
              {errors.age && touched.age ? (
                <div className={Styles.error_msg}>{errors.age}</div>
              ) : null}
              <Field
                id="age"
                name="age"
                placeholder="*Age"
                type="number"
                className={Styles.field}
              />
              {errors.email && touched.email ? (
                <div className={Styles.error_msg}>{errors.email}</div>
              ) : null}
              <Field
                id="email"
                name="email"
                type="email"
                placeholder="*Email"
                className={Styles.field}
              />
              {errors.password && touched.password ? (
                <div className={Styles.error_msg}>{errors.password}</div>
              ) : null}
              <Field
                id="password"
                name="password"
                placeholder="*Password"
                type="password"
                className={Styles.field}
              />
              {errors.confirmPass && touched.confirmPass ? (
                <div className={Styles.error_msg}>{errors.confirmPass}</div>
              ) : null}
              <Field
                id="confirmPass"
                name="confirmPass"
                type="password"
                placeholder="*Confirm Password"
                className={Styles.field}
              />
              <div className={Styles.userButtons}>
                <button type="submit" className={Styles.addButton}>
                  Edit User
                </button>
              </div>
            </Form>
          )}
        </Formik>
      ) : (
        <Formik
          initialValues={{
            userId: "",
            title: "",
            location: "",
            land: "",
            place: "",
            bedrooms: "",
          }}
          enableReinitialize
          onSubmit={(values) => {
            props.editHome(props.userId, values);
          }}
          validationSchema={AddHomeSchema}
        >
          {({ errors, touched }) => (
            <Form className={Styles.form}>
              {errors.userId && touched.userId ? (
                <div className={Styles.error_msg}>{errors.userId}</div>
              ) : null}
              <Field
                as="select"
                id="userId"
                name="userId"
                placeholder="*User Name"
                type="text"
                className={Styles.field}
              >
                <option value="">Select a User</option>
                {users}
              </Field>
              {errors.title && touched.title ? (
                <div className={Styles.error_msg}>{errors.title}</div>
              ) : null}
              <Field
                id="title"
                name="title"
                placeholder="*Title"
                type="text"
                className={Styles.field}
              />
              {errors.location && touched.location ? (
                <div className={Styles.error_msg}>{errors.location}</div>
              ) : null}
              <Field
                id="location"
                name="location"
                placeholder="*Location"
                type="text"
                className={Styles.field}
              />
              {errors.land && touched.land ? (
                <div className={Styles.error_msg}>{errors.land}</div>
              ) : null}
              <Field
                id="land"
                name="land"
                type="text"
                placeholder="*Land sqm"
                className={Styles.field}
              />
              {errors.place && touched.place ? (
                <div className={Styles.error_msg}>{errors.place}</div>
              ) : null}
              <Field
                id="place"
                name="place"
                placeholder="*Place sqm"
                type="text"
                className={Styles.field}
              />
              {errors.bedrooms && touched.bedrooms ? (
                <div className={Styles.error_msg}>{errors.bedrooms}</div>
              ) : null}
              <Field
                id="bedrooms"
                name="bedrooms"
                type="number"
                placeholder="*Bedrooms"
                className={Styles.field}
              />
              <div className={Styles.userButtons}>
                <button type="submit" className={Styles.addButton}>
                  Edit Home
                </button>
              </div>
            </Form>
          )}
        </Formik>
      )}
    </div>
  );
};

export default EditComponent;
