import "./App.css";
import { Route, Switch, NavLink } from "react-router-dom";
import UsersContainer from "./components/Users/container";
import HomesContainer from "./components/Homes/container";
import EditComponentContainer from "./components/Edit Component/container";
import AddComponentContainer from "./components/Add Component/container";

function App() {
  return (
    <div className="mx-auto container">
      <h3 className={"mx-auto pt-3"}>Wecome to STDev CRUD application</h3>
      <nav
        className={"navbar mx-auto navbar-expand w-100 "}
        id="navBar"
        style={{ backgroundColor: "white" }}
      >
        <ul className={"navbar-nav mr-auto pl-3"}>
          <li className={"nav-item px-2 h-100"}>
            <NavLink
              to={"/"}
              className={`nav-link h-100 d-flex align-items-center`}
            >
              Welcome
            </NavLink>
          </li>

          <li className={"nav-item px-2 h-100"}>
            <NavLink
              to={"/users"}
              className={`nav-link h-100 d-flex align-items-center`}
            >
              Users
            </NavLink>
          </li>

          <li className={"nav-item px-2 h-100"}>
            <NavLink
              to={"/homes"}
              className={`nav-link h-100 d-flex align-items-center`}
            >
              Homes
            </NavLink>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route exact path="/" render={() => <UsersContainer />} />
        <Route path="/users" render={() => <UsersContainer />} />
        <Route path="/homes" render={() => <HomesContainer />} />
        <Route path="/add" render={() => <AddComponentContainer />} />
        <Route path="/edit" render={() => <EditComponentContainer />} />
      </Switch>
    </div>
  );
}

export default App;
